# useful-scripts

Just some useful Python scripts I wrote.



## clean-history.py

To remove the duplicates from your `~/.bash_history` file, and if it contains the datetime of each command, the script will make sure to remove all datetimes which have no commands associated with them (since they were duplicates and got removed).

#### First let's create a directory to hold our files:

```
$ mkdir /tmp/my_history
```

#### then let's copy our existed `~/.bash_history`

```
$ cp ~/.bash_history > /tmp/my_history/history
```

#### Now let's remove all duplicates:

```
$ awk '!seen[$0]++' /tmp/my_history/history > /tmp/my_history/no_dups
```

#### if your `~/.bash_history` contains the date and time for the stored commands, then run the script:

```
$ python clean-history.py
```

If you want to use different names, you might want to modify the script `clean-history.py` first.


