""" first you need to generate a history with no duplicates
        $ mkdir /tmp/my_history
        $ cp ~/.bash_history > /tmp/my_history/history
        $ awk '!seen[$0]++' /tmp/my_history/history > /tmp/my_history/no_dups
    then run the script on the new generated history
        $ python clean-history.py """

from os.path import exists, isfile, isdir, dirname
from os import access, R_OK, W_OK
from re import match

filename = '/tmp/my_history/no_dups'
output = '/tmp/my_history/cleaned'
data = []
tmp = []


def file_is_readable(file_path):
    return exists(file_path) and isfile(file_path) and access(file_path, R_OK)

def dir_is_writable(dir_path):
    return exists(dir_path) and isdir(dir_path) and access(dir_path, W_OK)

def output_is_ok(file_path):
    """ return True if
            the file exists and writable
            or it`s directory is writable and file doesn`t exist
        Otherwise, return False """
    return ( file_is_readable(file_path) and access(file_path, W_OK) ) \
       or ( dir_is_writable(dirname(file_path)) and not exists(file_path) )

def is_cmd(line):
    pattern = r'^#(\d{10})'
    return not match(pattern, line)


if file_is_readable(filename):
    with open(filename, 'r') as lines:
        for line in lines:
            if is_cmd(line):
                latest_time = tmp[-1] if tmp else None
                data.append( (latest_time, line) )
                tmp = []
            else:   # is time not a command
                tmp.append(line)
    print('Was processed successfully!')
else:
    print('history was not processed! check the history path')
print(f'history: {filename}\n')

if output_is_ok(output):
    with open(output, 'w') as f:
        for entry in data:
            if entry[0] is not None:
                f.write(entry[0])
            f.write(entry[1])
    print('Was generated successfully!')
else:
    print('output was not generated! check the output path')
print(f'Output: {output}')

